package b137.obrera.s05a1;

public class Main {

    public static void main(String[] args) {
        Phonebook firstPerson = new Phonebook();

        firstPerson.setName("John Doe");

        firstPerson.getNumbers().add("+639152468596");
        firstPerson.getNumbers().add("+639228547963");
        firstPerson.getAddresses().add("my home in Quezon City");
        firstPerson.getAddresses().add("my office in Makati City");

        firstPerson.message();

        System.out.println();

        Phonebook secondPerson = new Phonebook();

        secondPerson.setName("Jane Doe");
        secondPerson.getNumbers().add("+639162148573");
        secondPerson.getNumbers().add("+639173698541");
        secondPerson.getAddresses().add("my home in Caloocan City");
        secondPerson.getAddresses().add("my office in Pasay City");

        secondPerson.message();
    }
}
