package b137.obrera.s05a1;

import java.util.ArrayList;

public class Phonebook extends Contact {

    private ArrayList<String> phonebookList = new ArrayList<String>();

    public Phonebook() { super(); }

    public Phonebook(String name, ArrayList<String> numbers, ArrayList<String> addresses, ArrayList<String> phonebookList) {
        super(name, numbers, addresses);
        this.phonebookList = phonebookList;
    }

    public void message() {
        if(super.getName() != null) {
            contactDetails();
        } else {
            System.out.println("Empty Phonebook!");
        }
    }
}
