package b137.obrera.s05a1;

import java.util.ArrayList;

public class Contact {

    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    public Contact() {}

    public Contact(String name, ArrayList<String> numbers, ArrayList<String> addresses) {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void contactDetails() {
        System.out.println(this.name);
        System.out.println("------------");
        System.out.println(this.name + " has the following registered numbers:");
        for(String number : numbers) {
            System.out.println(number);
        }
        System.out.println("--------------------------");
        System.out.println(this.name + " has the following registered addresses:");
        for(String address : addresses) {
            System.out.println(address);
        }
        System.out.println("==========================");
    }

}
